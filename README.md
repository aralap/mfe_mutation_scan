** Quick start **

1) modify mfe.py to add ViennaRNA python3 interface instalation path
See: https://www.tbi.univie.ac.at/RNA/documentation.html

2) run with:
python mfe.py <input_fasta_file> <folder_for_outputs>

Example :
python mfe.py /Users/aaptekmann/Downloads/ten_seqs.fasta ./Results
