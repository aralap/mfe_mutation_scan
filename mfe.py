import sys,time
# Folder for ViennaRNA python interface
# See https://www.tbi.univie.ac.at/RNA/documentation.html
sys.path.append('/Users/aaptekmann/Desktop/ViennaRNA-2.4.18/interfaces/Python3')

import RNA
from Bio import SeqIO


# Codon and Synonim Codon Table
table = {'AUA':'I', 'AUC':'I', 'AUU':'I', 'AUG':'M','ACA':'T', 'ACC':'T', 'ACG':'T', 'ACU':'T','AAC':'N', 'AAU':'N', 'AAA':'K', 'AAG':'K','AGC':'S', 'AGU':'S', 'AGA':'R', 'AGG':'R',                 'CUA':'L', 'CUC':'L', 'CUG':'L', 'CUU':'L','CCA':'P', 'CCC':'P', 'CCG':'P', 'CCU':'P','CAC':'H', 'CAU':'H', 'CAA':'Q', 'CAG':'Q','CGA':'R', 'CGC':'R', 'CGG':'R', 'CGU':'R','GUA':'V', 'GUC':'V', 'GUG':'V', 'GUU':'V','GCA':'A', 'GCC':'A', 'GCG':'A', 'GCU':'A','GAC':'D', 'GAU':'D', 'GAA':'E', 'GAG':'E','GGA':'G', 'GGC':'G', 'GGG':'G', 'GGU':'G','UCA':'S', 'UCC':'S', 'UCG':'S', 'UCU':'S','UUC':'F', 'UUU':'F', 'UUA':'L', 'UUG':'L','UAC':'Y', 'UAU':'Y', 'UAA':'_', 'UAG':'_','UGC':'C', 'UGU':'C', 'UGA':'_', 'UGG':'W'}
# Just keep this to generate it again if needed
# syn_table = { value : [ x for x in table.keys() if table[x] == value]  for value in set(table.values())}
syn_table = {'C': ['UGC', 'UGU'], 'M': ['AUG'], 'I': ['AUA', 'AUC', 'AUU'], 'K': ['AAA', 'AAG'], 'H': ['CAC', 'CAU'], 'D': ['GAC', 'GAU'], 'A': ['GCA', 'GCC', 'GCG', 'GCU'], 'Y': ['UAC', 'UAU'], 'G': ['GGA', 'GGC', 'GGG', 'GGU'], 'Q': ['CAA', 'CAG'], '_': ['UAA', 'UAG', 'UGA'], 'R': ['AGA', 'AGG', 'CGA', 'CGC', 'CGG', 'CGU'], 'N': ['AAC', 'AAU'], 'L': ['CUA', 'CUC', 'CUG', 'CUU', 'UUA', 'UUG'], 'T': ['ACA', 'ACC', 'ACG', 'ACU'], 'S': ['AGC', 'AGU', 'UCA', 'UCC', 'UCG', 'UCU'], 'F': ['UUC', 'UUU'], 'P': ['CCA', 'CCC', 'CCG', 'CCU'], 'E': ['GAA', 'GAG'], 'V': ['GUA', 'GUC', 'GUG', 'GUU'], 'W': ['UGG']}

def SaveOut(record,results,folder):
    h = open(folder+'/'+record.id+'.tsv','w')
    h.write('Position\tO_Codon\tM_Codon\tMFE\tDelta_MFE\tTranscript_ID\n')
    h.writelines(results)
    h.close()

def MutationScan(SeqRecord):
    print(f' Running mutation scan for {SeqRecord.id}.')
    start = time.time()
    # The RNA sequence
    seq = str(SeqRecord.seq.transcribe()) #"GACUAGUGGAACCAGGCUAUGUUUGUGACUCGCAGACUAACA"
    (ref_ss, ref_mfe) = RNA.fold(seq)
    results = []
    #Iterate each codon    
    for i in range(0,len(seq),3):
        codon = seq[i:i+3] 
        for synonim in syn_table[table[codon]]:
            if synonim != codon:
                # concat to generate temporal sequence
                temp_seq = seq[:i] + synonim + seq[i+3:] 
                # compute minimum free energy (MFE) and corresponding structure
                (ss, mfe) = RNA.fold(temp_seq)
            else:
                temp_seq = seq
                mfe = ref_mfe
            #output tsv line
            results.append(f'{i}\t{codon}\t{synonim}\t{mfe:.3f}\t{mfe-ref_mfe:.3f}\t{SeqRecord.id}\n')
    print(f'Finished {SeqRecord.id}, elapsed time {time.time()-start} ')
    return results

for record in SeqIO.parse(sys.argv[1], "fasta"):
    results = MutationScan(record)
    SaveOut(record, results, sys.argv[2])
    break
# print output
#print ("%s\n%s [ %6.2f ]" % (seq, ss, mfe))
